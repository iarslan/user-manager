<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'IWM User Manager',
    'description' => 'Frontend User Manager',
    'category' => 'plugin',
    'author' => 'Ibrahim Arslan',
    'author_email' => 'ibrahim-arslan@gmx.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
