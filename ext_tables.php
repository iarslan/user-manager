<?php

defined('TYPO3_MODE') || die();

(static function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'tx_usermanager_domain_model_user',
        'EXT:user_manager/Resources/Private/Language/locallang_csh_tx_usermanager_domain_model_user.xlf'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
        'tx_usermanager_domain_model_user'
    );
})();
