<?php

defined('TYPO3_MODE') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'UserManager',
        'Profile',
        [
            \IWM\UserManager\Controller\UserController::class => 'list, show, edit, update, delete, logout',
            \IWM\UserManager\Controller\CryptoListController::class => 'list, new, create, addCryptoToList'
        ],
        // non-cacheable actions
        [
            \IWM\UserManager\Controller\UserController::class => 'list, show, edit, update, delete, logout',
            \IWM\UserManager\Controller\CryptoListController::class => 'list, new, create, addCryptoToList'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'UserManager',
        'Login',
        [
            \IWM\UserManager\Controller\UserController::class => 'index, login'
        ],
        // non-cacheable actions
        [
            \IWM\UserManager\Controller\UserController::class => 'index, login'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'UserManager',
        'Register',
        [
            \IWM\UserManager\Controller\UserController::class => 'new, create'
        ],
        // non-cacheable actions
        [
            \IWM\UserManager\Controller\UserController::class => 'new, create'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'UserManager',
        'NewPassword',
        [
            \IWM\UserManager\Controller\UserController::class => 'newPassword, createPasswordLink, updatePassword'
        ],
        // non-cacheable actions
        [
            \IWM\UserManager\Controller\UserController::class => 'newPassword, createPasswordLink, updatePassword'
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    registration {
                        iconIdentifier = user_manager-plugin-registration
                        title = LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_user_manager_registration.name
                        description = LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_user_manager_registration.description
                        tt_content_defValues {
                            CType = list
                            list_type = usermanager_registration
                        }
                    }
                }
                show = *
            }
       }'
    );

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'user_manager-plugin-registration',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:user_manager/Resources/Public/Icons/user_plugin_registration.svg']
    );
})();

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][TYPO3\CMS\Extbase\Domain\Model\FrontendUser::class] = [
    'className' => \IWM\UserManager\Domain\Model\User::class,
];
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(
    'IWM\\UserManager\\Property\\TypeConverter\\UploadedFileReferenceConverter'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(
    'IWM\\UserManager\\Property\\TypeConverter\\ObjectStorageConverter'
);

// Register extended domain class
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        TYPO3\CMS\Extbase\Domain\Model\FrontendUser::class,
        \IWM\UserManager\Domain\Model\User::class
    );
