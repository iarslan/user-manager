<?php

namespace IWM\UserManager\System\Rest;

/**
 * Class CoinCap.
 */
class CoinCap extends Request implements ServiceInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'coinCap';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest($uri)
    {
        $additionalOptions = [
            'headers' => ['Accept-Encoding' => 'gzip']
        ];

        return new \GuzzleHttp\Psr7\Request(
            'GET',
            'https://api.coincap.io/v2/' . $uri,
            $additionalOptions
        );
    }
}
