<?php

namespace IWM\UserManager\System\Rest;

use Psr\Http\Message\RequestInterface;

/**
 * Interface ServiceInterface.
 */
interface ServiceInterface
{
    /**
     * @param string $uri
     *
     * @return RequestInterface
     */
    public function getRequest($uri);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param array $config
     */
    public function setConfig(array $config);
}
