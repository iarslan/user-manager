<?php

declare(strict_types=1);

namespace IWM\UserManager\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * CryptoAsset
 */
class CryptoAsset extends AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $assetId = '';

    /**
     * @return string
     */
    public function getAssetId(): string
    {
        return $this->assetId;
    }

    /**
     * @param string $assetId
     */
    public function setAssetId(string $assetId): void
    {
        $this->assetId = $assetId;
    }
}
