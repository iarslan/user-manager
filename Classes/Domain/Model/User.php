<?php

declare(strict_types=1);

namespace IWM\UserManager\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * User
 */
class User extends FrontendUser
{
    /**
     * profilePicture
     *
     * @var FileReference
     */
    protected $profilePicture = null;

    /**
     * birthDate
     *
     * @var string
     */
    protected $birthDate = '';

    /**
     * cryptoList
     *
     * @var CryptoList
     */
    protected $cryptoList = null;

    /**
     *
     * Returns the cryptoList
     *
     * @return CryptoList|null $cryptoList
     */
    public function getCryptoList(): ?CryptoList
    {
        return $this->cryptoList;
    }

    /**
     * Sets the cryptoList
     *
     * @param CryptoList $cryptoList
     * @return void
     */
    public function setCryptoList(CryptoList $cryptoList): void
    {
        $this->cryptoList = $cryptoList;
    }

    /**
     * Returns the profilePicture
     *
     * @return FileReference|null profilePicture
     */
    public function getProfilePicture(): ?FileReference
    {
        return $this->profilePicture;
    }

    /**
     * Sets the profilePicture
     *
     * @param FileReference $profilePicture
     * @return void
     */
    public function setProfilePicture(FileReference $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * Returns the birthDate
     *
     * @return string birthDate
     */
    public function getBirthDate(): string
    {
        return $this->birthDate;
    }

    /**
     * Sets the birthDate
     *
     * @param string $birthDate
     * @return void
     */
    public function setBirthDate(string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }
}
