<?php

declare(strict_types=1);

namespace IWM\UserManager\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * User
 */
class CryptoList extends AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * cryptoAssets
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IWM\UserManager\Domain\Model\CryptoAsset>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $asset = null;

    /**
     * __construct
     */
    public function __construct()
    {
        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * @return void
     */
    public function initializeObject(): void
    {
        $this->asset = $this->asset ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a assets
     *
     * @param CryptoAsset $asset
     * @return void
     */
    public function addAsset(CryptoAsset $asset): void
    {
        $this->asset->attach($asset);
    }

    /**
     * Removes an asset
     *
     * @param CryptoAsset $asset
     * @return void
     */
    public function removeAsset(CryptoAsset $asset): void
    {
        $this->asset->detach($asset);
    }

    /**
     * @return ObjectStorage<\IWM\UserManager\Domain\Model\CryptoAsset>
     */
    public function getAsset(): ?ObjectStorage
    {
        return $this->asset;
    }

    /**
     * @param ObjectStorage<\IWM\UserManager\Domain\Model\CryptoAsset> $asset
     * @return void
     */
    public function setAsset(ObjectStorage $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
