<?php

declare(strict_types=1);

namespace IWM\UserManager\Domain\Validator;

use IWM\UserManager\Domain\Model\User;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UserFormValidator extends AbstractValidator
{
    /**
     * Plugin Variables
     *
     * @var array
     */
    public $piVars = [];

    /**
     * Validator
     *
     * @param $newUser
     */
    protected function isValid($newUser)
    {
        $this->init();
        /** @var User $newUser */
        $firstName = $newUser->getFirstName();
        $lastName = $newUser->getLastName();
        $email = $newUser->getEmail();
        $birthDate = $newUser->getbirthDate();
        $password = $newUser->getPassword();

        if (empty($email) || !$this->validateEmail($email)) {
            $this->addError(
                $this->translateErrorMessage(
                    'validator.email.not_valid',
                    'user_manager'
                ),
                time()
            );
        }
        if (empty($birthDate) || !$this->validatebirthDate($birthDate)) {
            $this->addError(
                $this->translateErrorMessage(
                    'validator.dateOfBirth.not_valid',
                    'user_manager'
                ),
                time()
            );
        }
        if (empty($this->piVars['password_repeat']) || $this->piVars['password_repeat'] !== $password) {
            $this->addError(
                $this->translateErrorMessage(
                    'validator.password.repeat',
                    'user_manager'
                ),
                time()
            );
        }
    }
    /**
     * validate email
     *
     * @param $email
     * @return bool
     */
    protected function validateEmail($email)
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($email);
    }

    /**
     * validate birthDate format dd.mm.yy
     *
     * @param $birthdate
     * @return bool
     */
    protected function validatebirthDate($birthDate)
    {
        $pattern = '/^[0-9]{2}\.[0-9]{2}\.[0-9]{2}$/';
        return preg_match($pattern, $birthDate);
    }

    /**
     * Initialize Validator Function
     */
    protected function init()
    {
        $this->piVars = GeneralUtility::_GP('tx_usermanager_register') ??
            GeneralUtility::_GP('tx_usermanager_profile');
    }
}
