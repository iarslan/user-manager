<?php

declare(strict_types=1);

namespace IWM\UserManager\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * The repository for Users
 */
class UserRepository extends FrontendUserRepository
{
    protected $defaultOrderings = array(
        'last_name' => QueryInterface::ORDER_ASCENDING
    );

    //Repository settings
    public function initializeObject()
    {
        /** @var Typo3QuerySettings $querySettings */
        $querySettings = GeneralUtility::makeInstance(Typo3QuerySettings::class);
        // don't add the pid constraint
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
    }

    public function findUserByEmail($email)
    {
        $query = $this->createQuery();
        $query->matching($query->equals('email', $email));

        return $query->execute()->getFirst();
    }
}
