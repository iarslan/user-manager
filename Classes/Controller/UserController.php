<?php

declare(strict_types=1);

namespace IWM\UserManager\Controller;

use GuzzleHttp\Client;
use IWM\UserManager\Domain\Model\User;
use IWM\UserManager\Domain\Repository\UserRepository;
use IWM\UserManager\Property\TypeConverter\UploadedFileReferenceConverter;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Crypto\PasswordHashing\InvalidPasswordHashException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use IWM\UserManager\Utility\SessionUtility;
use IWM\UserManager\System\Rest\CoinCap;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * userRepository
     *
     * @var UserRepository
     */
    protected $userRepository = null;

    /**
     * @param UserRepository $userRepository
     */
    public function injectUserRepository(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function initializeAction()
    {
        //not logged in
        if ($this->request->getControllerActionName() == 'list' && empty(SessionUtility::getSessionData('login'))) {
            $this->redirect(
                'index',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'redirect.not_logged_in',
                        'user_manager',
                        null
                    ),
                    'alert' => 'alert-info'],
                $this->settings['loginPid']
            );
        }
    }

    public function listAction()
    {
        $user = $this->userRepository->findByUid(SessionUtility::getSessionData('login')['userId']);
        if (!empty($user)) {
            //logged in
            $this->view->assignMultiple([
                'user' => $user,
                'cryptocurrencies' => $this->getCryptoAssets('assets'),
                'message' => $this->request->getArguments()['message'],
                'alert' => $this->request->getArguments()['alert']
            ]);
        }
    }

    public function indexAction()
    {
        $arguments = $this->request->getArguments();
        //logged in
        if (!empty(SessionUtility::getSessionData('login'))) {
            $this->redirect(
                'list',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'redirect.logged_in',
                        'user_manager',
                        null
                    ),
                    'alert' => 'alert-info'
                ],
                $this->settings['profilePid']
            );
        } else {
            $this->view->assignMultiple([
                'message' => $arguments['message'],
                'alert' => $arguments['alert']
            ]);
        }
    }

    /**
     * action login
     *
     * @throws StopActionException|InvalidPasswordHashException
     */
    public function loginAction()
    {
        $args = $this->request->getArguments();
        $email = $args['email'];
        $password = $args['password'];

        if ($user = $this->userRepository->findUserByEmail($email)) {
            $passwordHash = $user->getPassword();
            $success = GeneralUtility::makeInstance(PasswordHashFactory::class)
                ->get($passwordHash, 'FE')
                ->checkPassword($password, $passwordHash);
            if ($success) {
                SessionUtility::setSessionData('login', ['userId' => $user->getUid()]);
                $this->redirect(
                    'list',
                    null,
                    null,
                    null,
                    $this->settings['profilePid']
                );
            } else {
                $this->redirect(
                    'index',
                    null,
                    null,
                    [
                        'message' => LocalizationUtility::translate(
                            'redirect.wrong_password',
                            'user_manager',
                            null
                        ),
                        'alert' => 'alert-danger'
                    ]
                );
            }
        } else {
            $this->redirect(
                'index',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'redirect.login_failed',
                        'user_manager',
                        null
                    ),
                    'alert' => 'alert-danger'
                ]
            );
        }
    }

    /**
     * action logout
     *
     * @throws StopActionException
     */
    public function logoutAction()
    {
        SessionUtility::destroySession('login');
        $this->redirect(
            'index',
            null,
            null,
            [
                'message' => LocalizationUtility::translate(
                    'redirect.logout',
                    'user_manager',
                    null
                ),
                'alert' => 'alert-info'
            ],
            $this->settings['loginPid']
        );
    }

    /**
     * action show
     *
     * @param User $user
     */
    public function showAction(User $user)
    {
        $this->view->assign('user', $user);
    }

    public function newPasswordAction()
    {
        $args = $this->request->getArguments();
        if ($args['new'] == 'newPassword') {
            $this->view->assignMultiple([
                'user' => $this->userRepository->findUserByEmail($args['email']),
                'args' => $args
            ]);
        } else {
            $this->view->assignMultiple([
                'message' => $args['message'],
                'alert' => $args['alert']
            ]);
        }
    }

    /**
     * action reset password
     *
     * @throws StopActionException|\Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function createPasswordLinkAction()
    {
        $args = $this->request->getArguments();
        $user = $this->userRepository->findUserByEmail($args['email']);

        if (!empty($user)) {
            $uriBuilder = $this->getControllerContext()->getUriBuilder();
            $uri = $uriBuilder->reset()
                ->uriFor(
                    'newPassword',
                    ['new' => 'newPassword', 'email' => $user->getEmail()],
                    'User',
                    'UserManager',
                    'newPassword'
                );
            $link = $this->request->getBaseUri() . $uri;
            $email = GeneralUtility::makeInstance(FluidEmail::class)
                ->to($args['email'])
                ->setTemplate('ResetPasswordLink')
                ->format('html')
                ->assign('link', $link);
            if ($GLOBALS['TYPO3_REQUEST'] instanceof ServerRequestInterface) {
                $email->setRequest($GLOBALS['TYPO3_REQUEST']);
            }
            GeneralUtility::makeInstance(Mailer::class)
                ->send($email);
            $this->redirect(
                'newPassword',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'user.sent.forgot_password',
                        'user_manager',
                        null
                    ) .
                    $user->getemail(),
                    'alert' => 'alert-success'
                ]
            );
        } else {
            $this->redirect(
                'newPassword',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'user.not_found',
                        'user_manager',
                        null
                    ),
                    'alert' => 'alert-info'
                ],
                $this->settings['loginPid']
            );
        }
    }

    public function newAction()
    {
        $arguments = $this->request->getArguments();
        $this->view->assignMultiple([
            'message' => $arguments['message'],
            'alert' => $arguments['alert']
        ]);
    }

    /**
     * @throws StopActionException
     * @throws IllegalObjectTypeException|InvalidPasswordHashException
     * @TYPO3\CMS\Extbase\Annotation\Validate("IWM\UserManager\Domain\Validator\UserFormValidator", param="newUser")
     */
    public function createAction(User $newUser)
    {
        if ($user = $this->userRepository->findUserByEmail($newUser->getEmail())) {
            $this->redirect(
                'new',
                null,
                null,
                ['message' => 'Email: ' . $user->getEmail() . ' wird bereits verwendet.', 'alert' => 'alert-info']
            );
        } else {
            $hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
            $hashedPassword = $hashInstance->getHashedPassword($newUser->getPassword());
            $newUser->setPassword($hashedPassword);
            $newUser->setPid((int) $this->settings['storagePid']);
            $this->userRepository->add($newUser);
            $this->redirect(
                'index',
                null,
                null,
                [
                    'message' => LocalizationUtility::translate(
                        'user.profile_registered',
                        'user_manager',
                        null
                    ),
                    'alert' => 'alert-success'
                ],
                $this->settings['loginPid']
            );
        }
    }

    /**
    * action edit
    *
    * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("user")
    */
    public function editAction(User $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * @TYPO3\CMS\Extbase\Annotation\Validate("IWM\UserManager\Domain\Validator\UserFormValidator", param="user")
     * @throws StopActionException|InvalidPasswordHashException
     */
    public function updateAction(User $user)
    {
        $hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
        $hashedPassword = $hashInstance->getHashedPassword($user->getPassword());
        $user->setPassword($hashedPassword);

        try {
            $this->userRepository->update($user);
        } catch (IllegalObjectTypeException | UnknownObjectException $e) {
            $this->addFlashMessage($e->getMessage());
        }

        $this->redirect(
            'list',
            null,
            null,
            ['message' => 'Profil bearbeitet.', 'alert' => 'alert-success'],
            $this->settings['profilePid']
        );
    }

    /**
     * @throws UnknownObjectException
     * @throws StopActionException
     * @throws IllegalObjectTypeException
     * @throws InvalidPasswordHashException
     */
    public function updatePasswordAction(User $user)
    {
        $hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
        $hashedPassword = $hashInstance->getHashedPassword($user->getPassword());
        $user->setPassword($hashedPassword);
        $persistenceManager = GeneralUtility::makeInstance(
            "TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager"
        );
        $this->userRepository->update($user);
        $persistenceManager->persistAll();

        $this->redirect(
            'index',
            null,
            null,
            [
                'message' => LocalizationUtility::translate(
                    'user.password.replaced',
                    'user_manager',
                    null
                ),
                'alert' => 'alert-success'
            ],
            $this->settings['loginPid']
        );
    }

    /**
     * action delete
     *
     * @throws StopActionException
     * @throws IllegalObjectTypeException
     */
    public function deleteAction(User $user)
    {
        $this->userRepository->remove($user);
        SessionUtility::destroySession('login');
        $this->redirect(
            'index',
            null,
            null,
            [
                'message' => LocalizationUtility::translate(
                    'user.profile.deleted',
                    'user_manager',
                    null
                ),
                'alert' => 'alert-success'
            ],
            $this->settings['loginPid']
        );
    }

    public function getCryptoAssets($uri)
    {
        $client = new Client();
        $coinCap = new CoinCap($client);
        $request = $coinCap->getRequest($uri);
        $response = $client->sendAsync($request)->wait();

        return json_decode($response->getBody()->getContents(), true);
    }

    protected function setTypeConverterConfigurationForImageUpload($argumentName)
    {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
            ->registerImplementation(
                \TYPO3\CMS\Extbase\Domain\Model\FileReference::class,
                \IWM\UserManager\Domain\Model\FileReference::class
            );

        $uploadConfiguration = [
            UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS =>
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
            ];
        /** @var PropertyMappingConfiguration $newExampleConfiguration */
        $newExampleConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
        $newExampleConfiguration->forProperty('profilePicture')
            ->setTypeConverterOptions(
                UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
        $newExampleConfiguration->forProperty('imageCollection.0')
            ->setTypeConverterOptions(
                UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
    }

    /**
    * Set TypeConverter option for image upload
    */
    public function initializeUpdateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('user');
    }

    /**
    * Set TypeConverter option for image upload
    */
    public function initializeCreateAction()
    {
        if (!empty($this->request->getArgument('newUser')['profilePicture']['type'])) {
            $this->setTypeConverterConfigurationForImageUpload('newUser');
        }
    }
}
