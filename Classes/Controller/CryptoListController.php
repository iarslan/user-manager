<?php

declare(strict_types=1);

namespace IWM\UserManager\Controller;

use IWM\UserManager\Domain\Model\CryptoAsset;
use IWM\UserManager\Domain\Model\CryptoList;
use IWM\UserManager\Domain\Model\User;
use IWM\UserManager\Domain\Repository\CryptoAssetRepository;
use IWM\UserManager\Domain\Repository\CryptoListRepository;
use IWM\UserManager\Domain\Repository\UserRepository;
use IWM\UserManager\System\Rest\CoinCap;
use IWM\UserManager\Utility\SessionUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use GuzzleHttp\Client;

/**
 * This file is part of the "IW User Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */

/**
 * UserController
 */
class CryptoListController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * cryptoListRepository
     *
     * @var CryptoListRepository
     */
    protected $cryptoListRepository = null;

    /**
     * userRepository
     *
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * cryptoAssetRepository
     *
     * @var CryptoAssetRepository
     */
    protected $cryptoAssetRepository = null;

    public function injectCryptoAssetRepository(CryptoAssetRepository $cryptoAssetRepository)
    {
        $this->cryptoAssetRepository = $cryptoAssetRepository;
    }

    public function injectCryptoListRepository(CryptoListRepository $cryptoListRepository)
    {
        $this->cryptoListRepository = $cryptoListRepository;
    }

    public function injectUserRepository(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * action list
     */
    public function listAction()
    {
        $user = $this->userRepository->findByUid(SessionUtility::getSessionData('login')['userId']);
        if (!$user->getCryptoList()) {
            $this->view->assignMultiple([
                'message' => ['text' => 'Keine Liste vorhanden!', 'alert' => 'alert-info'],
                'user' => $user
            ]);
        } else {
            /** @var User $user */
            foreach ($user->getCryptoList()->getAsset() as $asset) {
                $userAssets[] = $this->getCryptoAsset('assets/' . $asset->getAssetId());
            }
            $this->view->assignMultiple([
                'cryptoList' => $user->getCryptoList()->getName(),
                'userAssets' => $userAssets
            ]);
        }
    }

    /**
     * action new
     */
    public function newAction()
    {
        $arguments = $this->request->getArguments();

        $this->view->assignMultiple([
            'message' => $arguments['message'],
            'alert' => $arguments['alert'],
            'user' => $this->userRepository->findByUid(SessionUtility::getSessionData('login')['userId'])
        ]);
    }

    /**
     * create new crypto list
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function createAction(CryptoList $cryptoList, User $user)
    {
        $user->setCryptoList($cryptoList);
        $this->userRepository->update($user);
        $this->cryptoListRepository->add($cryptoList);
    }

    /**
     * ajax call
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function addCryptoToListAction()
    {
        $arguments = $this->request->getArguments();
        $user = $this->userRepository->findByUid(SessionUtility::getSessionData('login')['userId']);
        /** @var User $user */
        $asset = new CryptoAsset();
        $asset->setAssetId($arguments['cryptocurrency']['id']);
        $user->getCryptoList()->addAsset($asset);
        $this->cryptoListRepository->add($user->getCryptoList());
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
        $objectManager->persistAll();
    }

    public function getCryptoAsset($uri)
    {
        $client = new Client();
        $coinCap = new CoinCap($client);
        $request = $coinCap->getRequest($uri);
        $response = $client->sendAsync($request)->wait();

        return json_decode($response->getBody()->getContents(), true);
    }
}
