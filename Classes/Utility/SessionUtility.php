<?php

declare(strict_types=1);

namespace IWM\UserManager\Utility;

use IWM\UserManager\Domain\Model\User;

/**
 * Class SessionUtility
 * @codeCoverageIgnore
 */
class SessionUtility
{
    /**
     * @param $key
     * @return void
     */
    public static function destroySession($key)
    {
        $GLOBALS['TSFE']->fe_user->setKey('ses', $key, null);
    }

    /**
     * @param $key
     * @param $data
     * @return void
     */
    public static function setSessionData($key, $data)
    {
        $GLOBALS['TSFE']->fe_user->setKey('ses', $key, $data);
    }

    /**
     * @param string|null $key
     * @param mixed $default
     * @return mixed
     */
    public static function getSessionData(string $key = null, $default = null)
    {
        return $GLOBALS['TSFE']->fe_user->getKey('ses', $key) ?? $default;
    }
}
