<?php

declare(strict_types=1);

namespace IWM\UserManager\Tests\Unit\Domain\Model;

use IWM\UserManager\Domain\Model\CryptoAsset;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Test case.
 *
 * @author Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */
class CryptoAssetTest extends UnitTestCase
{
    /**
     * @var CryptoAsset
     */
    private $subject = null;

    protected function setUp(): void
    {
        $this->subject = new CryptoAsset();
    }

    /**
     * @test
     */
    public function isAbstractEntity(): void
    {
        self::assertInstanceOf(AbstractEntity::class, $this->subject);
    }

    /**
     * @test
     */
    public function getAssetIdInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getAssetId());
    }

    /**
     * @test
     */
    public function setAssetIdSetsAssetId(): void
    {
        $value = 'bitcoin';
        $this->subject->setAssetId($value);

        self::assertSame($value, $this->subject->getAssetId());
    }
}
