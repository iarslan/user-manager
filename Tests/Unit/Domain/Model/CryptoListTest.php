<?php

declare(strict_types=1);

namespace IWM\UserManager\Tests\Unit\Domain\Model;

use IWM\UserManager\Domain\Model\CryptoAsset;
use IWM\UserManager\Domain\Model\CryptoList;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Test case.
 *
 * @author Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */
class CryptoListTest extends UnitTestCase
{
    /**
     * @var CryptoList
     */
    private $subject = null;

    protected function setUp(): void
    {
        $this->subject = new CryptoList();
    }

    /**
     * @test
     */
    public function isAbstractEntity(): void
    {
        self::assertInstanceOf(AbstractEntity::class, $this->subject);
    }

    /**
     * @test
     */
    public function getNameInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getName());
    }

    /**
     * @test
     */
    public function setNameSetsName(): void
    {
        $value = 'Watchlist';
        $this->subject->setName($value);

        self::assertSame($value, $this->subject->getName());
    }

    /**
     * @test
     */
    public function addAssetToObjectStorageHoldingAsset(): void
    {
        $asset = new CryptoAsset();
        $assetObjectStorageMock = $this->getMockBuilder(ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $assetObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($asset));
        $this->subject->_setProperty('asset', $assetObjectStorageMock);

        $this->subject->addAsset($asset);
    }

    /**
     * @test
     */
    public function removeAssetFromObjectStorageHoldingAsset(): void
    {
        $asset = new CryptoAsset();
        $assetObjectStorageMock = $this->getMockBuilder(ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $assetObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($asset));
        $this->subject->_setProperty('asset', $assetObjectStorageMock);

        $this->subject->removeAsset($asset);
    }

    /**
     * @test
     */
    public function getAssetReturnsInitialValueForAsset(): void
    {
        $newObjectStorage = new ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getAsset()
        );
    }

    /**
     * @test
     */
    public function setAssetForObjectStorageContainingAssetSetsAsset(): void
    {
        $asset = new CryptoAsset();
        $objectStorageHoldingExactlyOneAsset = new ObjectStorage();
        $objectStorageHoldingExactlyOneAsset->attach($asset);
        $this->subject->setAsset($objectStorageHoldingExactlyOneAsset);

        self::assertEquals($objectStorageHoldingExactlyOneAsset, $this->subject->_getProperty('asset'));
    }
}
