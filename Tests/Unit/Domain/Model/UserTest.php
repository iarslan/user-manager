<?php

declare(strict_types=1);

namespace IWM\UserManager\Tests\Unit\Domain\Model;

use IWM\UserManager\Domain\Model\CryptoList;
use IWM\UserManager\Domain\Model\User;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Test case.
 *
 * @author Ibrahim Arslan <ibrahim-arslan@gmx.de>
 */
class UserTest extends UnitTestCase
{
    /**
     * @var User
     */
    private $subject = null;

    protected function setUp(): void
    {
        $this->subject = new User();
    }

    /**
     * @test
     */
    public function isAbstractEntity(): void
    {
        self::assertInstanceOf(AbstractEntity::class, $this->subject);
    }

    /**
     * @test
     */
    public function getFirstNameInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getFirstName());
    }

    /**
     * @test
     */
    public function setFirstNameSetsFirstName(): void
    {
        $value = 'John';
        $this->subject->setFirstName($value);

        self::assertSame($value, $this->subject->getFirstName());
    }

    /**
     * @test
     */
    public function getLastNameInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getLastName());
    }

    /**
     * @test
     */
    public function setLastNameSetsLastName(): void
    {
        $value = 'Doe';
        $this->subject->setLastName($value);

        self::assertSame($value, $this->subject->getLastName());
    }

    /**
     * @test
     */
    public function getEmailInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getEmail());
    }

    /**
     * @test
     */
    public function setEmailSetsEmail(): void
    {
        $value = 'john.doe@web.de';
        $this->subject->setEmail($value);

        self::assertSame($value, $this->subject->getEmail());
    }

    /**
     * @test
     */
    public function getDateOfBirthInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getBirthDate());
    }

    /**
     * @test
     */
    public function setDateOfBirthSetsDateOfBirth(): void
    {
        $value = '10.05.90';
        $this->subject->setBirthDate($value);

        self::assertSame($value, $this->subject->getBirthDate());
    }

    /**
     * @test
     */
    public function getPasswordInitiallyReturnsEmptyString(): void
    {
        self::assertSame('', $this->subject->getPassword());
    }

    /**
     * @test
     */
    public function setPasswordSetsPasswords(): void
    {
        $value = 'loremipsum';
        $this->subject->setPassword($value);

        self::assertSame($value, $this->subject->getPassword());
    }

    /**
     * @test
     */
    public function getProfilePictureInitiallyReturnsNull(): void
    {
        self::assertNull($this->subject->getProfilePicture());
    }

    /**
     * @test
     */
    public function setProfilePictureSetsProfilePicture(): void
    {
        $model = new FileReference();
        $this->subject->setProfilePicture($model);

        self::assertSame($model, $this->subject->getProfilePicture());
    }

    /**
     * @test
     */
    public function getCryptoListInitiallyReturnsNull(): void
    {
        self::assertNull($this->subject->getCryptoList());
    }

    /**
     * @test
     */
    public function setCryptoListSetsCryptoList(): void
    {
        $model = new CryptoList();
        $this->subject->setCryptoList($model);

        self::assertSame($model, $this->subject->getCryptoList());
    }
}
