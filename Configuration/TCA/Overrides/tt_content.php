<?php

defined('TYPO3_MODE') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'UserManager',
    'Profile',
    'Profile'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'UserManager',
    'Login',
    'Login'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'UserManager',
    'NewPassword',
    'NewPassword'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'UserManager',
    'Register',
    'Register'
);
