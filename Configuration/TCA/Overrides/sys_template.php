<?php

defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'user_manager',
    'Configuration/TypoScript',
    'IWM User Manager'
);
