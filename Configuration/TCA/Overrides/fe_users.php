<?php

defined('TYPO3_MODE') || die();
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$tmp_user_manager_columns = [
    'profile_picture' => [
        'exclude' => true,
        'label' => 'LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_usermanager_domain_model_user.profile_picture',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'profile_picture',
            [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'foreign_types' => [
                    '0' => [
                        'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                        'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                    ],
                ],
                'foreign_match_fields' => [
                    'fieldname' => 'profile_picture',
                    'tablenames' => 'fe_users',
                    'table_local' => 'sys_file',
                ],
                'maxitems' => 1
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),

    ],
    'birth_date' => [
        'label' => 'LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_usermanager_domain_model_user.birth_date',
        'config' => [
            'type' => 'input',
        ],
    ],
    'crypto_list' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_usermanager_domain_model_user.crypto_list',
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'tx_usermanager_domain_model_cryptolist',
            'maxitems' => 1
        ],

    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_user_manager_columns);

$showItem = ',--div--;LLL:EXT:user_manager/Resources/Private/Language/locallang_db.xlf:tx_usermanager_domain_model_user,';
$showItem .= 'crypto_list, birth_date, profile_picture,';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', $showItem);
