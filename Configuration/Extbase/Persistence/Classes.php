<?php

declare(strict_types=1);

return [
    \IWM\UserManager\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
        'properties' => [
            'uid_local' => [
                'fieldName' => 'originalFileIdentifier',
            ],
        ],
    ],
    \IWM\UserManager\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'recordType' => \IWM\UserManager\Domain\Model\User::class,
        'properties' => [
            'birthDate' => [
                'fieldName' => 'birth_date',
            ],
            'profilePicture' => [
                'fieldName' => 'profile_picture',
            ],
            'cryptoList' => [
                'fieldName' => 'crypto_list',
            ],
        ],
    ],
    \In2code\Femanager\Domain\Model\User::class => [
        'tableName' => \In2code\Femanager\Domain\Model\User::TABLE_NAME,
        'properties' => [
            'terms' => [
                'fieldName' => 'tx_femanager_terms'
            ],
            'termsDateOfAcceptance' => [
                'fieldName' => 'tx_femanager_terms_date_of_acceptance'
            ]
        ]
    ],
];
