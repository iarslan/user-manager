var service = {
    ajaxCall: function (url, resultId) {
        $.ajax({
            url: url,
            resultId: resultId,
            cache: false,
            data: {url: url},
            method: 'POST',
            success: function () {
                $('#result_' + resultId).html('added to list').fadeIn('fast');
            },
            error: function (jqXHR, textStatus, errorThrow) {
                $('#result_' + resultId).html('Ajax request - ' + textStatus + ': ' + errorThrow).fadeIn('fast');
            }
        });
    }
};

$(document).on('click', '#addCryptoToList', function (ev) {
    var url = $(this).attr('action');
    var resultId = $(this).attr('data-result-id');
    ev.preventDefault();
    service.ajaxCall(url, resultId);
});
