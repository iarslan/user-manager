CREATE TABLE fe_users (
	profile_picture int(11) unsigned NOT NULL DEFAULT '0',
	birth_date varchar(255) NOT NULL DEFAULT '',
    crypto_list int(11) DEFAULT '0' NOT NULL,
);

CREATE TABLE tx_usermanager_domain_model_cryptolist (
    name varchar(255) NOT NULL DEFAULT '',
    asset int(11) unsigned DEFAULT '0' NOT NULL,
);

CREATE TABLE tx_usermanager_domain_model_cryptoasset (
    asset_id varchar(255) NOT NULL DEFAULT '',
    list int(11) unsigned DEFAULT '0' NOT NULL,
);


