# Frontend User Registration and Management. In addition, list cryptocurrencies and add to watchlist 

## Status
- In development 

## Requirements
 - TYPO3 10
 - PHP 7.4

## Plugin Configuration
Set page uid's in constants.typoscript:
```
loginPid =
profilePid =
newPasswordPid =
storagePid =
```

## Mail Configuration

Set Template and Layout in MAIL LocalConfiguration.php\
Example:
```
'templateRootPaths' => [
    '700' => 'EXT:user_manager/Resources/Private/Templates/Email'
],
'layoutRootPaths' => [
    '700' => 'EXT:user_manager/Resources/Private/Layouts'
],
```
